package org.sessl

import org.jamesii.mlrules.simulator.factory.AdvancedHybridSimulatorFactory.Integrator

object DictyExperiment extends App {

  import sessl._
  import sessl.mlrules._

  var configs: List[Simulator] = List(
	//new AdvancedHybridSimulator(Integration method, single stochastic reaction per step, initial r, minimum r, epsilon min, epsilon max, epsilon reject)
	// configuration 1
	new AdvancedHybridSimulator(Integrator.DORMAND_PRINCE, true, 0.01, 0.01, Double.MinValue, Double.MaxValue, Double.MaxValue),
	// configuration 2
	new AdvancedHybridSimulator(Integrator.DORMAND_PRINCE, true, 0.1, 0.1, Double.MinValue, Double.MaxValue, Double.MaxValue),
	// configuration 3
 	new AdvancedHybridSimulator(Integrator.DORMAND_PRINCE, true, 0.01, 0.001, 0.01, 0.05, 0.1),
	// configuration 4
	new AdvancedHybridSimulator(Integrator.DORMAND_PRINCE, false, 0.01, 0.001, 0.01, 0.05, 0.1)
  )

  for (config <- configs) {
	  for (gridSize <- List(5/*,7,10*/)) {
		val exp = new Experiment with ParallelExecution with Observation with CSVOutput with TimeMeasurement {
		  val grid = 1 to gridSize
		  model = "model.mlrj"
		  simulator = config
		  replications = 40
		  stopTime = 5000
		  set("xmax" <~ grid.size, "ymax" <~ grid.size)
		  parallelThreads = 1
		  observeAt(range(0, 100, stopTime))
		  observeSimulator
		  for (x <- grid; y <- grid)
			observe(s"$x-$y" ~ s"System/GRID($x.0, $y.0)/CELL")
		  withExperimentResult(writeCSV)
		}
		execute(exp)
	  }
  }
}