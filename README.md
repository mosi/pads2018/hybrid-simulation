# ReadMe to reproduce the results

This readme contains the basic information to reproduce the results of the experiments in the paper 
"Hybrid Simulation of Dynamic Reaction Networks in Multi-Level Models" by Tobias Helms, Pia Wilsdorf, and Adelinde M. Uhrmacher.

All artifacts can be downloaded [here](https://git.informatik.uni-rostock.de/mosi/pads2018/hybrid-simulation/repository/master/archive.zip).

The repository for the artifacts is [https://git.informatik.uni-rostock.de/mosi/pads2018/hybrid-simulation](https://git.informatik.uni-rostock.de/mosi/pads2018/hybrid-simulation).

#### Description of the artifacts

- model.mlrj: contains the dicty model implemented in ML-Rules
- experiment.scala: contains the experiment description implemented in SESSL
- repo: contains all needed libraries to execute the experiemnts
- pom.xml: needed by Maven to identify all dependencies and start the correct executables
- script.R: contains R code to analyze and plot generated data
- example: contains exemplary results with grid size 5, plots generated with the available R methods and a list of the according function calls

#### System requirements:
- Java Runtime Environment (JRE) 8 (https://www.java.com)
- Maven 3 (at least version 3.3.1)
  - Downloads: https://maven.apache.org/download.cgi
  - Installation manual: https://maven.apache.org/install.html
- R to generate plots of experiment results (https://www.r-project.org/)
  - Installation and Administration manual: https://cran.r-project.org/doc/manuals/r-release/R-admin.html
  - IDE RStudio: https://www.rstudio.com/

#### Further information:

- For more information about ML-Rules, see the [ML-Rules repository](https://git.informatik.uni-rostock.de/mosi/mlrules2)
- For more information about SESSL, see [sessl.org](http://sessl.org)
- The default experiment description of this repository only executes experiments with a grid size 5, the grid sizes 7 and 10 are commented
- Executing experiments with the grid sizes 7 and 10 take many hours and need much memory, ensure that enough space is available for the JVM
- In case an experiment stops due to memory problems, reduce the number of parallel threads in the SESSL experiment

#### Start the experiments:

- Windows: execute run.cmd
- Linux: execute run.sh
- Maven is automatically downloading all needed dependencies and additional software
- The current experiment.scala is used for the experiment: the number of replications, simulation end time, simulator configurations etc. can be set there
  
#### Analysis of results:

- For each simulator configuration and grid size defined in the *experiment.scala* file, one results directory named according to the timestamp of its creation will be created
  - Each results directory contains one *config-0* directory with the results of the replications
    - The file *config.csv* contains the used grid size of the according replications
    - One file *run-x.csv* contain the model and simulator results for one replication
  - *Attention*: There is no information in the directories to identify the used simulator configuration
    - Since the simulator configurations are processed sequentially (only replications of one simulator configuration are potentially executed in parallel), a mapping of directories and simulator configuration can be identified (e.g., the results directory with the smallest timestamp corresponds to the first simulator configuration defined in the experiment description)
    - When using various simulator configurations and grid sizes in one experiment, the following scheme is processed:
      - All grid sizes are processed for one simulator configuration and then the next simulator configuration is considered etc.
      - For example, when using four simulator configurations and three grid sizes, the three results directories with the smallest timestamps refer to the results of first simulator configuration and the three grid sizes.
    - We plan to extend SESSL to improve the handling of simulator configurations
    - See the example directory for an example of generated directories and a concrete experiment description
- The R script contains various methods to analyze the data and to reproduce plots according to the plots shown in the paper:
  - See the example directory to see exemplary function calls of the methods and the generated plots
  - The packages **plot3D** and **Hmisc** have to be installed and loaded in R to generate the plots with the following commands:
    - *install.packages("plot3D")*
    - *library(plot3D)*
    - *install.packages('Hmisc')*
    - *library(Hmisc)*
  - **createPDFTimes(dir, gridSize, times, rows, cols)**
    - create a PDF *plotsTimes.pdf* considering one *results* directory and a set of simulation time points of the replications
    - for each time point, a 2D map of the amoeba distribution is created for each replication
    - one page of the PDF has *rows* rows and *cols* columns
  - **createPDFConfigs(dirs, gridSize, time, rows, cols)**
    - create a PDF *plotsConfigs.pdf* considering a set of *results* directories and one simulation time point of the replications
    - for results directory, a 2D map of the amoeba distribution is created for each replication
    - one page of the PDF has *rows* rows and *cols* columns
  - **plotSD(config1,config2,config3,config4,errBarsAt)**
    - create a PDF *plotSD.pdf* showing the amoeba aggregation of four simulator configurations for all observed simulation time points
    - the variables *config1/config2/config3/config4* shall refer to results directories
    - the variable *errBarsAt* can be used to set simulation time points for shown error bars
    - plots according to Figure 4 in the paper can be created with this method
  - **plotRuntime(dirs)**
    - create a PDF *runtime.pdf* showing the boxplot of runtimes of the given results directories
    - plots according to Figure 6 in the paper can be created with this method
  - **plotSimAttribute(dirs,n)**
    - create a PDF *simAttribute.pdf* showing the boxplot of the chosen simulator attribute of the given results directories
    - n = 1: average value of the variable r
    - n = 2: the number of simulation steps where at least one stochastic reaction has been executed
    - n = 3: the number of rejects
    - n = 4: the runtime in milliseconds
    - n = 5: the total number of executed stochastic reactions
    - n = 6: the total number of simulation steps (including steps without stochastic reactions)
  - **plotNumReactions(config1Dirs,config2Dirs,config3Dirs,config4Dirs,gridValues)**
    - create a PDF *reactions.pdf* for four simulator configurations and three grid sizes
    - not flexible for other numbers of configurations or grid sizes
    - plots according to Figure 7 in the paper can be created with this method
  - **plotDetailedAggregation(config1,config2,config3,config4)**
    - create a PDF *detailedAggregation.pdf* for four simulator configurations showing the detailed amoeba distribution of the last simulation time observation
    - plots according to Figure 5 cin the paper can be created with this method