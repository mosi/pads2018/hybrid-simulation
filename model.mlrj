//Dictyostelium aggregation model in discrete space

// PARAMETERS
scale:1;
// default: one cell per grid position
xmax:5; // change the grid size here
ymax:5; // change the grid size here
kd_dicty:2/1000;
kd_dicty2:0.001;
//kd_camp:2.4e6/(1000*1000);
kd_camp:2.4e6/(1000*1000);
nA:6.023e23;
v:3.6720e-14;
k1:2.0;
k2:0.9/nA/(v*scale)/1e-6;
k3:2.5;
k4:1.5;
k5:0.6;
k6:0.8/nA/(v*scale)/1e-6;
k7:1.0*nA*(v*scale)*1e-6;
k8:1.3/nA/(v*scale)/1e-6;
k9:0.3;
k10:0.8/nA/(v*scale)/1e-6;
k11:0.7;
k12:4.9;
k13:23.0;
k14:4.5;
init_cAMPe:1100*scale;
init_cAMPi:4100*scale;
init_ACA:7300*scale;
init_PKA:7100*scale;
init_ERK2:2500*scale;
init_RegA:3000*scale;
init_CAR1:6000*scale;

initialize :: num -> num -> sol;
initialize 0 y = [];
initialize x 0 = initialize(x - 1, ymax);
initialize x y = GRID(x,y)[
                   init_cAMPe CAMPe
                   + 1 CELL[
                     init_cAMPi CAMPi
                     + init_ACA ACA
                     + init_PKA PKA
                     + init_ERK2 ERK2
                     + init_RegA RegA
                     + init_CAR1 CAR1
                   ]
                 ]
                 + initialize(x, y - 1);


// SPECIES DEFINITIONS
System()[];
GRID(num,num)[];
CELL()[];
CAMPe();
CAMPi();
ACA();
PKA();
ERK2();
RegA();
CAR1();

// INITIAL SOLUTION
>>INIT[
  System[initialize(xmax, ymax)]
];

// REACTION RULES

// intra-cellular dynamics
// the following rules are computed continuously
CAR1:c -> ACA + CAR1 @ k1*#c;
ACA:a + PKA:p -> PKA @ k2*#a*#p;
CAMPi:a -> PKA + CAMPi @ k3*#a;
PKA:p -> @ k4*#p;
CAR1:c -> ERK2 + CAR1 @ k5*#c;
PKA:p + ERK2:e -> PKA @ k6*#p*#e;
CELL[s?]:c -> CELL[RegA + s?] @ k7*#c;
ERK2:e + RegA:r -> ERK2 @ k8*#e*#r;
ACA:a -> CAMPi + ACA @ k9*#a;
RegA:r + CAMPi:a -> RegA @ k10*#r*#a;
CELL[ACA:a + s?] -> CAMPe + CELL[ACA + s?] @ k11*#a;
CAMPe:a -> @ k12*#a;
GRID(x,y)[CAMPe:a + CELL[c?] + g?] -> GRID(x,y)[CAMPe + CELL[CAR1 + c?] + g?] @ k13*#a/(1 + cells) where cells = count(g?,'CELL');
CAR1:c -> @ k14*#c;

// movement of cell to adjacent position depending on external cAMP amount
GRID(x1,y1)[CELL[c?] + CAMPe:a1 + g?] + GRID(x2,y2)[CAMPe:a2 + g2?] -> GRID(x1,y1)[CAMPe + g?] + GRID(x2,y2)[CELL[c?] + CAMPe + g2?] @ if ((#a2>#a1) && (#a1 > 0) && validCoords) then kd_dicty*(#a2*(1/#a1)) else 0 where validCoords = ((x1!=x2) && (y1!=y2)) && ((x1-x2<=1)&&(x1-x2>=-1)) && ((y1-y2<=1)&&(y1-y2>=-1));
//spontaneous movement
GRID(x1,y1)[CELL[c?] + g?] + GRID(x2,y2)[g2?] -> GRID(x1,y1)[g?] + GRID(x2,y2)[CELL[c?] + g2?] @ if (validCoords) then kd_dicty2 else 0 where validCoords = ((x1!=x2) && (y1!=y2)) && ((x1-x2<=1)&&(x1-x2>=-1)) && ((y1-y2<=1)&&(y1-y2>=-1));

// cAMP diffusion
// the following rules are computed continuously
GRID(x,y)[CAMPe:a + sol?] + GRID(x,y2)[sol2?] -> GRID(x,y)[sol?] + GRID(x,y2)[CAMPe + sol2?]  @ if (y +1 == y2) then kd_camp*#a else 0;
GRID(x,y)[CAMPe:a + sol?] + GRID(x2,y2)[sol2?] -> GRID(x,y)[sol?] + GRID(x2,y2)[CAMPe + sol2?]@ if ((x+1 == x2) && (y+1 == y2)) then kd_camp*#a else 0;
GRID(x,y)[CAMPe:a + sol?] + GRID(x2,y)[sol2?] -> GRID(x,y)[sol?] + GRID(x2,y)[CAMPe + sol2?]   @ if (x + 1 == x2) then kd_camp*#a else 0;
GRID(x,y)[CAMPe:a + sol?] + GRID(x2,y2)[sol2?] -> GRID(x,y)[sol?] + GRID(x2,y2)[CAMPe + sol2?] @ if ((x + 1 == x2) && (y - 1 == y2)) then kd_camp*#a else 0;
GRID(x,y)[CAMPe:a + sol?] + GRID(x,y2)[sol2?] -> GRID(x,y)[sol?] + GRID(x,y2)[CAMPe + sol2?]   @ if (y - 1 == y2) then kd_camp*#a else 0;
GRID(x,y)[CAMPe:a + sol?] + GRID(x2,y2)[sol2?] -> GRID(x,y)[sol?] + GRID(x2,y2)[CAMPe + sol2?] @ if ((x - 1 == x2) && (y - 1 == y2)) then kd_camp*#a else 0;
GRID(x,y)[CAMPe:a + sol?] + GRID(x2,y)[sol2?] -> GRID(x,y)[sol?] + GRID(x2,y)[CAMPe + sol2?]  @ if (x - 1 == x2) then kd_camp*#a else 0;
GRID(x,y)[CAMPe:a + sol?] + GRID(x2,y2)[sol2?] -> GRID(x,y)[sol?] + GRID(x2,y2)[CAMPe + sol2?] @ if (x - 1 == x2) && (y + 1 == y2) then kd_camp*#a else 0;
