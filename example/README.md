# Exemplary analysis of the results and creation of the plots

In the *example* directory, an experiment description is available that has been used to create the four *results* directories. The following list describes the plots in the directory and according function calls to create those plots:
The working directory in R has been set to the *example* directory.
The colors are chosen according to the paper (configuration 1: black, configuration 2: red, configuration 3: green, configuration 4: blue)

- plotsTimes.pdf: 
  - createPDFTimes("./results-20180308-113538/",5,seq(0,5000,100),5,4)
  - show the movement process of the amoebas for each replication of the given results directory for simulation times 0,100,200,...,5000
- plotsConfigs.pdf:
  - createPDFConfigs(c("./results-20180308-113538/","./results-20180308-120039/","./results-20180308-120838/","./results-20180308-121320/"),5,5000,5,4)
  - show the amoeba distribution of all replications of the four given results directories for simulation time 5000
- plotSD.pdf
  - plotSD("./results-20180308-113538/","./results-20180308-120039/","./results-20180308-120838/","./results-20180308-121320/",seq(1000,5000,1000))
  - show the amoeba aggregation of the four results directories for all observed simulation times
- runtime.pdf
  - plotRuntime(c("./results-20180308-113538/","./results-20180308-120039/","./results-20180308-120838/","./results-20180308-121320/"))
  - show the runtime boxplot 
- simAttribute.pdf
  - plotSimAttribute(c("./results-20180308-113538/","./results-20180308-120039/","./results-20180308-120838/","./results-20180308-121320/"),5)
  - show the boxplot for n=5, i.e., the total number of stochastic reactions is shown
    - the distribution of all configurations are similar supporting that all configurations make similar movements
- reactions.pdf
  - plotNumReactions(c("./results-20180308-113538/","./results-20180308-113538/","./results-20180308-113538/"),c("./results-20180308-120039/","./results-20180308-120039/","./results-20180308-120039/"),c("./results-20180308-120838/","./results-20180308-120838/","./results-20180308-120838/"),c("./results-20180308-121320/","./results-20180308-121320/","./results-20180308-121320/"),c(25,25,25))
  - since only results of grid size = 5 are available in the example:
    - the same results directories are used for each configuration
    - this plot only shows the reaction step distributions at one amoeba point (gridValue = 25)
- detailedAggregation.pdf
  - plotDetailedAggregation("./results-20180308-113538/","./results-20180308-120039/","./results-20180308-120838/","./results-20180308-121320/")
  - show the a histogram for the amoeba distribution at simulation time 5000 for the four given results directories